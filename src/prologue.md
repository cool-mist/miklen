# Prologue

Meko woke up. Again. It is dark outside. It always is. Meko doesn’t remember looking at any other color outside the window. 

His body fits barely on his bed. His room looks the same as it was a few hours ago when he last slept. He is the only person in the room. The room is dimly lit by a purple bulb in the ceiling. There are no switches to control it. Initially it was a bit difficult to sleep with the lights on. After a few days, his eyes didn’t care. Right beside the bed, on the left, there is a cabinet. In the middle of the blue cabinet is a small red colored outline of a Crajki'it's face with its pink horns and a speech bubble from its mouth with the words “FOOD”.

Ever since the day Meko undertook this mission, he has been staying in this room. He sleeps. He wakes up. He eats. If one should wonder why he is content with this space that is only slightly larger than the bed he sleeps in, one should simply accept without questions that this is what the mission requires. That was his debrief eight years ago.

A secret mission. 

A mission so secret, even Meko doesn’t know what he is supposed to do. He was told he would be able to figure it out when the time comes.

Meko opened his eyes. The mirror in the ceiling reflected his face.
The pink horns above his head is glowing orange. He is hungry. But before he can get the Hunger tablets from his cabinet, he needs to attend to the large blinking screen on his left. The screen on this left has been showing the 3 green triangles for the past 6 months and was never blinking. In the bottom right, there is a small white square with the text “0.8c”. Today, there are only 2 triangles. The 3rd triangle is now a square.

Meko yells “FOOD”. The cabinet opens and dispenses 5 white colored tables. He puts them into his mouth. With a nervous gulp, he wonders what the change on the screen means.

Is it finally time ?

