# Chapter 1 #

_"It's that time of the year again..."_, Nuklo sighed as he rose from his bed

He decided to get dressed for the yearly ritual as fast as possible. 
He didn't want the Army at the doorstep again. He quickly moved towards the washroom.
His washroom has a tap providing running water whenever he wants because
Nuklo is much higher ranked than most of the other citizens as per his kingdom's Klaas
hierarchy. 20 minutes later, he is already dressed up in his yellow shirt and a
dark red suit that marks him as a Veinier citizen. He
doesn't have to fight in any real battles, yet. He is still under training, as
are other Veiniers of his kingdom, ever since the conquest. He picks up his zamulet
and ties it over his head. 30 minutes from now, at 3 am, he is going to march in
a procession along with the Army. The march signifies an anniversary of the
Independence of Miklen. But more importantly serves as a display of the Army's
power should anyone revolt against the present government. The last ones who
fought for an overthrow five and a half years ago were impaled to death. Their
skin carefully removed and their bodies hanged in Kokra'ath. Today only
their skeletons remain. This did a good job in encouraging everyone else to 
cooperate with the Army.

Six years ago, today, the Crajki'it took over the land of Miklen, adding it to
their small collection of slave lands. The Rule Book that accompanied
their conquest, dictated both the life of living as well as the form of the non-living.
The first among those rules was the Klaas system. Every citizen of the new
Kingdom of Crajki'it were pigeonholed into 5 groups. Nuklo is a Veinier, the third of
them that is supposed to specialize in fighting. On normal days, Veiniers would
only need to maintain law and order. If required, they would be drafted into one
of the many wars the Crajki'it's wage towards other areas of the planet.

Nuklo wore his red shoes and started walking towards his
scooter. He needed to be at the ground in another 15 minutes. It was a 2 minute
ride from his home. He turned the switch to find the HUD flash with a battery
icon. He let out a growl because now he has to walk a couple of leagues.
_Probably jog to make it in time._ He looked up at the sky, at the larger than usual moon
and let his thoughts wander towards Pre-Independence, a time before the battle.

---

Nuklo was not in that battle. He hated any form of confrontation. Still does. He was busy at his home,
some 5000 leagues away from the battlefield, composing and writing music. He
would occasionally play his piano. Most of his time was spent writing orchestral
music for the local band. His teacher was the only musician in his urrage and
when he died ten years ago, Nuklo took over the position of the town musician.
Others in his urrage either thought too little of music as a job, or thought
too much of it and considered something else to do for themselves. Every few weeks, there
would be some event where the towsnfolk would want to enjoy a few hours with 
music. Luckily, his teacher had already handpicked a group of three
individuals who were good enough to read musical sheets Nuklo disburses 
and also play them almost accurately.

Kakri played the drums. Ever since he started his schooling from the neighbouring urrage,
he had been fascinated by the drums. His fondest memory is when he had sneaked into the
music room to play the drums during a mathematics lecture. It didn't make any noise
because these were of the newer electric kind. Determined to play something,
he ended up breaking the drum pads and receiving a lecture in discipline
instead. When he was ten years old, his father discontinued his schooling due to
financial reasons, and the school gave him the broken drums as a parting gift.
He later fixed them with Nuklo's master's help and by the time he was eighteen,
was trained by the same master to read and follow written music. His parents were
happy when he was hired into the local band. It makes about 1000 jeeks per month
and that was enough for Kakri to get by without his parent's money.

Hakem played the electric guitar. While his wife fell for his guitar skills,
she is disappointed over his decision to remain a guitarist in a local band. Nobody
knows much of his upbringing, except that he once pulled a tiger's whisker as a
kid. Hakem maintains that this was his origin story towards playing the guitar.

The most depressed person in the urrage was probably Getop. She lost her parents when she
was ten to a freak accident involving a shortcircuited battery of an elcar.
Growing up in an orphanage, she never grew out of her loss. In a bid to help her
survive life, Nuklo's master gave her a bass guitar and taught her to play the
notes. The money that she makes right now, and the two friends that she has from
the band occasionally make her smile.

Nuklo was busy writing his next song in his sound-proofed room when his father 
informed him of the fact that Jehabro'oth, the king, was killed by a group
called Sedrams at the battlefield. Nuklo didn't care too much about it at that
time because a king was overthrown every few years or so in Miklen. But he knew
this time was different when few days later a messenger talked to the urrage head 
who immediately called for destroying most of the commercial buildings and public
roads and called for rebuilding it according to a new blueprint from a rule
book. The Book apparently also banned any form of music. Nuklo was therefore under
a crisis when the red suited minotaur informed Nuklo that he is being drafted into
the Veiniers. He hasn't seen or heard of his parents, or his band ever since.

---

Nuklo entered the streets leading to the marching field.
He could see the black suited Herghuls standing at the intersection of 
the streets surrounding the ground.

_"Native Veinier scum - 4th street"_ a Herghul spoke in her mic that
was attached to her collar. Another Herghul a short distance away moved his
forefinger from his ear, looked towards her and nodded.

Nuklo looked towards the moon again and briefly considered if it grew in size.
Before the war, classism and racism, though conceivable and sometimes practised,
were not the norm as it was now a days. The upper Klaases contained very few
natives for the Crajki'its fear, rightfully so, a rebellion or some form of instability
in the government at the very least. The natives were even less in the Klaas
that killed the king. The Sedrams occupied the pinnacle of the Klaas. Serving as an 
elite strike force in battles that Veiniers and Herghuls cannot finish. Shrouded
in mystery, lot of rumours spread across Crajki'it of their nature. Some say
that the Sedrams are capable of using magic. Nuklo has also heard that the
Sedrams have no face beneath their green shrouds.

He slowly walked inside the field now towards his group. The leader of his platoon
waved his hands at him and shouted something. He knew it was some other racial slur 
and so started jogging towards him fast. The Herghul leader turned around and 
started speaking to another of his Klaas. He went and stood in his designated 
squad of Veiniers in that platoon. Something was bothering him today. The night
felt strange.

Nobody spoke in the squad. It was not permitted for anyone below Herghul to talk
in the presence of other Herghul's. His squad had 10 Veiniers, all
natives of Miklen before the war. He was surprised to see that there were no women
in this particular squad he was part of.

Nuklo suddenly felt his body lose some weight.

There was a loud horn, signalling the start of the march. All Veiniers who
couldn't make it in time will be incinerated by the Herghul hekuzens later. A force
field shot up near the borders of the ground. A few minutes later, the stands
surrounding the ground started emerging from below. The other lower Klaases have to
arrive few hours back and populate the spectator stands underground to avoid
conjestion when the Herghuls and Veiniers arrive.

_"Crajki'it"_ - the leader shouted, brought his closed fist near his chest and
looked up.

Another wave and Nuklo felt his body become even more lighter.

That was odd, this never happens when the spectator stands rise up before a march.
He had a feeling that everyone else around him also felt the same way 
because all of them broke their stance briefly. _Maybe this was a new module
being tested out by the Herghuls_. 

_"Crajki'it"_ - all members of all his squads followed the leader.

Everyone put their hands and heads down after the roar except Nuklo. A Herghul 
walked towards Nuklo with his hekuzen primed. Before she could shoot, there was
another one of those sweeping waves.
It was accompanied by a soft noise of a single drop of water dripping from a leaky tap.
The Herghul lost balance momentarily and dropped her weapon. Nuklo felt completely weightless now. Picking up her weapon, the Herghul looked ahead and saw all the Veiniers float a couple of arm lengths in the air!!

_"Th.. The Mooo..."_

Nuklo fainted and floated mid-air before he could complete the sentence. 

